def say_hello(name=None):
    if name is None:
        return "Hello, World!!"
    return f"Hello, {name}"
